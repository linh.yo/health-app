export interface IRecordSet {}

export interface ISetItem {
  id: number
  image: string
  title: string
  description: string
  link: string
}

export interface IBodyRecord {}

export interface IMyExercise {}

export interface IMyDiary {}

export interface IExerciseItem {
  id: number
  content: string
  duration: number
  kilocalories: number
}

export interface IDiaryItem {
  id: number
  content: string
  date: string
  time: string
}
