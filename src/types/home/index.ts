export interface IHomeCarousel {}

export interface IHomeChart {}

export interface IHomeCategory {
  filterMenu: (value: string) => void
}

export interface ICategoryItem {
  id: number
  name: string
  icon?: JSX.Element,
  isChecked: boolean
}

export interface IHomeMenu {
  filterKey: string
}

export interface IMenuItem {
  id: number
  image: string
  category: string
  label: string
}
