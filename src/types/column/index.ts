export interface IColumnCategory {}

export interface ICategoryItem {
  id: number
  title: string
  text: string
}

export interface IColumnNews {}

export interface INewsItem {
  id: number
  image: string
  date: string
  time: string
  title: string
  tags: Array<string>
}
