import React from 'react'
import { IColumnCategory, ICategoryItem } from 'types/column'
import { Row, Col } from 'react-bootstrap'
import Title from 'components/Title'

const categoryList: ICategoryItem[] = [
  {
    id: 1,
    title: 'Column',
    text: 'オススメ'
  },
  {
    id: 2,
    title: 'Diet',
    text: 'ダイエット'
  },
  {
    id: 3,
    title: 'Beauty',
    text: '美容'
  },
  {
    id: 4,
    title: 'Health',
    text: '健康'
  }
]

const ColumnCategory: React.FC<IColumnCategory> = () => {

  return (
    <section className="column-category">
      <Row className="row-32">
        {
          categoryList?.map((item, index) =>
            <Col xs={12} sm={6} md={3} key={index}>
              <div className="category-item">
                <Title variant="h2">
                  Recommended <br />
                  {item.title}
                </Title>
                <Title variant="h3">
                  {item.text}
                </Title>
              </div>
            </Col>
          )
        }
      </Row>
    </section>
  )
}

export default ColumnCategory
