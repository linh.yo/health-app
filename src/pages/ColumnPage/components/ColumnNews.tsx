import React, { useState, useEffect } from 'react'
import { IColumnNews, INewsItem } from 'types/column'
import { Row, Col, Button, Spinner } from 'react-bootstrap'
import { useLoading } from 'utils/hooks/useLoading'
import { newsItems } from 'constants/data'
import Title from 'components/Title'
import { Link } from 'react-router-dom'
import { PATHS } from 'constants/paths'

const ColumnNews: React.FC<IColumnNews> = () => {

  const [newsList, setNewsList] = useState<INewsItem[]>([])
  const [{ isLoading }, { start, stop }] = useLoading()

  useEffect(() => {
    setNewsList(newsItems)
  }, [])

  const loadMoreItem = () => {
    start()
    setTimeout(() => {
      stop()
      const newList = newsList.concat(newsList)
      setNewsList(newList)
    }, 500)
  }

  return (
    <section className="column-news">
      <Row className="row-8">
        {
          newsList.map((item, index) =>
            <Col xs={12} sm={6} md={3} key={index}>
              <div className="news-item" data-aos="fade-up" data-aos-delay="100">
                <Link to={PATHS.COLUMN_PAGE + '/' + item.id}>
                  <div className="news-image">
                    <div className="img" style={{ backgroundImage: `url(${item.image})` }}/>
                    <div className="news-date-time">
                      <span className="me-3">{item.date}</span>
                      <span>{item.time}</span>
                    </div>
                  </div>
                </Link>
                <div className="news-text">
                  <Link to={PATHS.COLUMN_PAGE + '/' + item.id}>
                    <Title variant="h4">
                      {item.title}
                    </Title>
                  </Link>
                  <ul className="news-tag">
                    {
                      item.tags.map((tag, idx) =>
                        <li key={idx}>
                          <Link to={PATHS.COLUMN_PAGE + '/' + item.id}>
                            #{tag}
                          </Link>
                        </li>
                      )
                    }
                  </ul>
                </div>
              </div>
            </Col>
          )
        }
      </Row>

      <div className="text-center">
        <Button className="menu-button btn-read-more" onClick={loadMoreItem}>
          {
            isLoading ?
              <Spinner as="span"
                       animation="border"
                       role="status"
              />
              :
              <>コラムをもっと見る</>
          }
        </Button>
      </div>
    </section>
  )
}

export default ColumnNews
