import React from 'react'
import { Container } from 'react-bootstrap'
import ColumnCategory from './components/ColumnCategory'
import ColumnNews from './components/ColumnNews'

const ColumnPage = () => {

  return (
    <article className="column-page">
      <Container>

        <ColumnCategory />

        <ColumnNews />

      </Container>
    </article>
  )
}

export default ColumnPage
