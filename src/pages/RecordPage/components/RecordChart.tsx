import React from 'react'
import { LineProps, ResponsiveLine } from '@nivo/line'
import { chartOptions } from 'constants/config'

const RecordChart: React.FC<LineProps> = ({ data }) => {

  return (
    <section className="record-chart">
      <ResponsiveLine {...chartOptions}
                      data={data}
                      yScale={{ type: "linear", stacked: false }}
                      margin={{ top: 30, right: 30, bottom: 40, left: 30 }}
      />
    </section>
  )
}

export default RecordChart
