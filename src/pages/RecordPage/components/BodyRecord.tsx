import React, { useState } from 'react'
import { IBodyRecord } from 'types/record'
import Title from 'components/Title'
import RecordChart from './RecordChart'
import { Nav, Tab } from 'react-bootstrap'
import { dayData, monthData, weekData, yearData } from 'constants/data'

const BodyRecord: React.FC<IBodyRecord> = () => {

  const [key, setKey] = useState<string>('year')

  const selectTab = (eventKey: any) => {
    setKey(eventKey)
  }

  const tabList = [
    {
      id: 1,
      key: "day",
      title: "日"
    },
    {
      id: 2,
      key: "week",
      title: "週"
    },
    {
      id: 3,
      key: "month",
      title: "月"
    },
    {
      id: 4,
      key: "year",
      title: "年"
    }
  ]

  const data: {[index: string]:any} = {
    day: dayData,
    week: weekData,
    month: monthData,
    year: yearData
  }

  return (
    <section className="body-record">
      <div className="d-flex">
        <Title variant="h4" className="record-title">
          Body <br /> Record
        </Title>
        <Title variant="h3" className="record-day">
          2021.05.21
        </Title>
      </div>
      <Tab.Container defaultActiveKey={key}>
        <Tab.Content>
          <Tab.Pane eventKey={key} className="active">
            <RecordChart data={data[key]} />
          </Tab.Pane>
        </Tab.Content>
        <Nav>
          {
            tabList.map((item, index) =>
              <Nav.Link key={index}
                        eventKey={item.key}
                        onClick={() => selectTab(item.key)}
                        className={`${item.key === key ? 'active' : ''}`}
              >
                {item.title}
              </Nav.Link>
            )
          }
        </Nav>
      </Tab.Container>
    </section>
  )
}

export default BodyRecord
