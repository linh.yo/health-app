import React from 'react'
import { IRecordSet, ISetItem } from 'types/record'
import { Row, Col, Image } from 'react-bootstrap'
import { Link } from 'react-scroll'
import Image_1 from 'assets/images/sample/MyRecommend-1.png'
import Image_2 from 'assets/images/sample/MyRecommend-2.png'
import Image_3 from 'assets/images/sample/MyRecommend-3.png'
import Title from 'components/Title'
import Text from 'components/Text'

const setList: ISetItem[] = [
  {
    id: 1,
    image: Image_1,
    title: 'Body Record',
    description: '自分のカラダの記録',
    link: 'body-record'
  },
  {
    id: 2,
    image: Image_2,
    title: 'My Exercise',
    description: '自分の運動の記録',
    link: 'my-exercise'
  },
  {
    id: 3,
    image: Image_3,
    title: 'My Diary',
    description: '自分の日記',
    link: 'my-diary'
  }
]

const RecordSet: React.FC<IRecordSet> = () => {

  return (
    <section className="record-set">
      <Row className="row-48">
        {
          setList.map((item, index) =>
            <Col key={index} xs={12} sm={4}>
              <Link to={item.link}
                    spy smooth
                    duration={300}
                    offset={-64}>
                <div className="set-item" data-aos="zoom-in" data-aos-delay="100">
                  <div className="set-image">
                    <Image src={item.image} alt={item.title} />
                  </div>
                  <div className="set-text">
                    <Title variant="h2">
                      {item.title}
                    </Title>
                    <Text>
                      {item.description}
                    </Text>
                  </div>
                </div>
              </Link>
            </Col>
          )
        }
      </Row>
    </section>
  )
}

export default RecordSet
