import React, { useState, useEffect } from 'react'
import { IExerciseItem, IMyExercise } from 'types/record'
import { Row, Col } from 'react-bootstrap'
import Title from 'components/Title'
import Text from 'components/Text'
import { exerciseData } from 'constants/data'

const MyExercise: React.FC<IMyExercise> = () => {

  const [exercises, setExercises] = useState<IExerciseItem[]>()

  useEffect(() => {
    setExercises(exerciseData)
  }, [])

  return (
    <section className="my-exercise">
      <div className="d-flex mb-2">
        <Title variant="h4" className="record-title">
          My Exercise
        </Title>
        <Title variant="h3" className="record-day">
          2021.05.21
        </Title>
      </div>
      <div className="exercise-list">
        <Row className="row-40">
          {
            exercises?.map((item, index) =>
              <Col xs={12} sm={6} key={index}>
                <div className="exercise-item">
                  <div>
                    <Text className="exercise-content">{item.content}</Text>
                    <Text className="exercise-kilocalories">{item.kilocalories}kcal</Text>
                  </div>
                  <div className="text-end">
                    <Text className="exercise-time">{item.duration} min</Text>
                  </div>
                </div>
              </Col>
            )
          }
        </Row>
      </div>
    </section>
  )
}

export default MyExercise
