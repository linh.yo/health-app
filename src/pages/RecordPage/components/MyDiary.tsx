import React, {useEffect, useState} from 'react'
import { IMyDiary, IDiaryItem } from 'types/record'
import { Row, Col, Button, Spinner } from 'react-bootstrap'
import Title from 'components/Title'
import { diaryData } from 'constants/data'
import { useLoading } from 'utils/hooks/useLoading'

const MyDiary: React.FC<IMyDiary> = () => {

  const [diaries, setDiaries] = useState<IDiaryItem[]>()
  const [{ isLoading }, { start, stop }] = useLoading()

  useEffect(() => {
    setDiaries(diaryData)
  }, [])

  const loadMoreItem = () => {
    start()
    setTimeout(() => {
      stop()
      const newList = diaries?.concat(diaries)
      setDiaries(newList)
    }, 500)
  }

  return (
    <section className="my-diary">
      <Title variant="h3">
        My Diary
      </Title>

      <Row className="row-12">
        {
          diaries?.map((item, index) =>
            <Col xs={12} sm={6} md={4} lg={3} className="" key={index}>
              <div className="diary-item" data-aos="zoom-in" data-aos-delay="100">
                <div className="d-flex">
                  <Title variant="h4" className="me-3">
                    {item.date}
                  </Title>
                  <Title variant="h4">
                    {item.time}
                  </Title>
                </div>
                <div className="diary-content" dangerouslySetInnerHTML={{__html: item.content}} />
              </div>
            </Col>
          )
        }
      </Row>

      <div className="text-center">
        <Button className="btn-read-more" onClick={loadMoreItem}>
          {
            isLoading ?
              <Spinner as="span"
                       animation="border"
                       role="status"
              />
              :
              <>記録をもっと見る</>
          }
        </Button>
      </div>
    </section>
  )
}

export default MyDiary
