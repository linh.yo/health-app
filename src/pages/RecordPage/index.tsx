import React from 'react'
import { Container } from 'react-bootstrap'
import RecordSet from './components/RecordSet'
import BodyRecord from './components/BodyRecord'
import MyExercise from './components/MyExercise'
import MyDiary from './components/MyDiary'

const RecordPage = () => {

  return (
    <article className="record-page">
      <Container>

        <RecordSet />

        <BodyRecord />

        <MyExercise />

        <MyDiary />

      </Container>
    </article>
  )
}

export default RecordPage
