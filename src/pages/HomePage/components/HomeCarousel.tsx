import React, {useState} from 'react'
import { Carousel } from 'react-bootstrap'
import { IHomeCarousel } from 'types/home'
import ProgressBar from 'components/ProgressBar'
import Image_1 from 'assets/images/sample/d01.jpg'
import Image_2 from 'assets/images/sample/d02.jpg'
import Image_3 from 'assets/images/sample/l01.jpg'
import Image_4 from 'assets/images/sample/l02.jpg'
import Image_5 from 'assets/images/sample/l03.jpg'
import Image_6 from 'assets/images/sample/m01.jpg'
import Image_7 from 'assets/images/sample/m02.jpg'
import Image_8 from 'assets/images/sample/m03.jpg'

const HomeCarousel: React.FC<IHomeCarousel> = () => {

  const interval = 2000
  const [hidden, setHidden] = useState<boolean>(true)
  const carouselList = [
    Image_1, Image_2, Image_3, Image_4, Image_5, Image_6, Image_7, Image_8
  ]

  const onSlid = () => {
    setHidden(true)
  }

  const onSlide = () => {
    setHidden(false)
  }

  return (
    <section className="home-carousel">
      <Carousel interval={2000} fade
                indicators={false}
                controls={false}
                onSlid={onSlid}
                onSlide={onSlide}>
        {
          carouselList.map((item, index) =>
            <Carousel.Item key={index}>
              <div className="carousel-image">
                <div style={{ backgroundImage: `url(${item})` }}/>
              </div>
              {
                hidden &&
                <Carousel.Caption>
                  {index + 1} / {carouselList.length}
                </Carousel.Caption>
              }
            </Carousel.Item>
          )
        }
      </Carousel>

      {
        hidden && <ProgressBar interval={interval} />
      }
    </section>
  )
}

export default HomeCarousel
