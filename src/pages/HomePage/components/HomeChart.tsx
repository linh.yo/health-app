import React from 'react'
import { IHomeChart } from 'types/home'
import { ResponsiveLine } from '@nivo/line'
import { chartOptions } from 'constants/config'
import { yearData } from 'constants/data'
import useWindowDimensions from 'utils/hooks/useWindowDimensions'

const HomeChart: React.FC<IHomeChart> = () => {

  const { width } = useWindowDimensions()

  return (
    <section className="home-chart">
      <ResponsiveLine {...chartOptions}
                      data={yearData}
                      yScale={{ type: "linear", stacked: false }}
                      margin={{ top: 30, right: width < 600 ? 30 : 100, bottom: 50, left: width < 600 ? 30 : 50 }}
      />
    </section>
  )
}

export default HomeChart
