import React, { useEffect, useState } from 'react'
import { IHomeCategory, ICategoryItem } from 'types/home'
import { IconMeal, IconSnack } from 'components/Icon'
import { Row } from 'react-bootstrap'

const categoryList: ICategoryItem[] = [
  {
    id: 1,
    name: 'Morning',
    icon: IconMeal,
    isChecked: false
  },
  {
    id: 2,
    name: 'Lunch',
    icon: IconMeal,
    isChecked: false
  },
  {
    id: 3,
    name: 'Dinner',
    icon: IconMeal,
    isChecked: false
  },
  {
    id: 4,
    name: 'Snack',
    icon: IconSnack,
    isChecked: false
  }
]

const HomeCategory: React.FC<IHomeCategory> = ({ filterMenu }) => {

  const [filters, setFilters] = useState<ICategoryItem[]>(categoryList)

  const onFilter = (e: React.ChangeEvent<HTMLInputElement>) => {
    const {target: { value, checked }} = e;

    setFilters(state =>
      state.map(f => {
        if (f.name === value) {
          return {
            ...f,
            isChecked: checked
          }
        } else {
          return {
            ...f,
            isChecked: false
          }
        }
      })
    )

    if (checked) {
      filterMenu(value)
    } else {
      filterMenu('*')
    }
  }

  useEffect(() => {
  }, [])

  return (
    <section className="home-category">
      <Row className="justify-content-center">
        {
          filters.map((item, index) =>
            <div className="category-item" key={index}>
              <input id={item.name}
                     type="checkbox"
                     value={item.name}
                     onChange={onFilter}
                     checked={item.isChecked}
              />
              <label htmlFor={item.name}>
                <div>
                  <i>{item.icon}</i>
                  <p>{item.name}</p>
                </div>
              </label>
            </div>
          )
        }
      </Row>
    </section>
  )
}

export default HomeCategory
