import React, { useRef, useState, useEffect } from 'react'
import { IHomeMenu, IMenuItem } from 'types/home'
import { Row, Col, Button, Spinner } from 'react-bootstrap'
import Isotope from 'isotope-layout'
import Image_1 from 'assets/images/sample/m01.jpg'
import Image_2 from 'assets/images/sample/l03.jpg'
import Image_3 from 'assets/images/sample/d01.jpg'
import Image_4 from 'assets/images/sample/l01.jpg'
import Image_5 from 'assets/images/sample/m02.jpg'
import Image_6 from 'assets/images/sample/l02.jpg'
import Image_7 from 'assets/images/sample/d02.jpg'
import Image_8 from 'assets/images/sample/s01.jpg'
import { useLoading } from 'utils/hooks/useLoading'

const items:IMenuItem[] = [
  {
    id: 1,
    image: Image_1,
    category: 'Morning',
    label: '05.21.Morning'
  },
  {
    id: 2,
    image: Image_2,
    category: 'Lunch',
    label: '05.21.Lunch'
  },
  {
    id: 3,
    image: Image_3,
    category: 'Dinner',
    label: '05.21.Dinner'
  },
  {
    id: 4,
    image: Image_4,
    category: 'Snack',
    label: '05.21.Snack'
  },
  {
    id: 5,
    image: Image_5,
    category: 'Morning',
    label: '05.20.Morning'
  },
  {
    id: 6,
    image: Image_6,
    category: 'Lunch',
    label: '05.20.Lunch'
  },
  {
    id: 7,
    image: Image_7,
    category: 'Dinner',
    label: '05.20.Dinner'
  },
  {
    id: 8,
    image: Image_8,
    category: 'Snack',
    label: '05.21.Snack'
  }
]

const HomeMenu: React.FC<IHomeMenu> = ({ filterKey }) => {

  const isotope = useRef<any>()
  const [menuList, setMenuList] = useState<IMenuItem[]>(items)
  const [{ isLoading }, { start, stop }] = useLoading()

  useEffect(() => {
    isotope.current = new Isotope('.filter-container', {
      itemSelector: '.filter-item',
      layoutMode: 'fitRows',
    })
    return () => isotope.current.destroy()
  }, [menuList])

  useEffect(() => {
    filterKey === '*'
      ? isotope.current.arrange({filter: `*`})
      : isotope.current.arrange({filter: `.${filterKey}`})
  }, [filterKey, menuList])

  const loadMoreItem = () => {
    start()
    setTimeout(() => {
      stop()
      const newList = menuList.concat(menuList)
      setMenuList(newList)
    }, 500)
  }

  return (
    <section className="home-menu">
      <Row className="filter-container row-8">
        {
          menuList.map((item, index) =>
            <Col key={index} xs={6} md={4} lg={3} className={`filter-item ${item.category}`}>
              <div className="menu-item" data-aos="zoom-in" data-aos-delay="100">
                <div className="menu-image">
                  <div style={{ backgroundImage: `url(${item.image})` }}/>
                </div>
                <div className="menu-text">
                  {item.label}
                </div>
              </div>
            </Col>
          )
        }
      </Row>

      <div className="text-center">
        <Button className="menu-button btn-read-more" onClick={loadMoreItem}>
          {
            isLoading ?
              <Spinner as="span"
                       animation="border"
                       role="status"
              />
              :
              <>記録をもっと見る</>
          }
        </Button>
      </div>
    </section>
  )
}

export default HomeMenu
