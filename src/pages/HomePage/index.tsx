import React, { useState } from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import HomeCarousel from './components/HomeCarousel'
import HomeChart from './components/HomeChart'
import HomeCategory from './components/HomeCategory'
import HomeMenu from './components/HomeMenu'

const HomePage = () => {

  const [filterKey, setFilterKey] = useState<string>('*')

  const filterMenu = (category: string) => {
    setFilterKey(category)
  }

  return (
    <article className="home-page">
      <Row className="row-0">
        <Col xs={12} sm={5}>
          <HomeCarousel />
        </Col>
        <Col xs={12} sm={7}>
          <HomeChart />
        </Col>
      </Row>
      <Container>
        <HomeCategory filterMenu={filterMenu} />

        <HomeMenu filterKey={filterKey} />
      </Container>
    </article>
  )
}

export default HomePage
