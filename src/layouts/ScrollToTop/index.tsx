import React, { useState, useEffect } from 'react'
import { IconArrowUpCircle } from 'components/Icon'

const ScrollToTop = () => {

  const [showTopBtn, setShowTopBtn] = useState<boolean>(false);
  useEffect(() => {
    window.addEventListener('scroll', () => {
      if (window.scrollY > 400) {
        setShowTopBtn(true)
      } else {
        setShowTopBtn(false)
      }
    })
  }, [])

  const goToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    })
  }

  return (
    <section className="scroll-to-top">
      {
        showTopBtn &&
        <i onClick={goToTop}>
          {IconArrowUpCircle}
        </i>
      }
    </section>
  )
}

export default ScrollToTop
