import React from 'react'
import { Container, Nav } from 'react-bootstrap'
import { PATHS } from 'constants/paths'
import { INav } from 'types/common'

const Footer = () => {

  const navList:INav[] = [
    {
      name: '会員登録',
      path: PATHS.CUSTOM_PAGE
    },
    {
      name: '運営会社',
      path: PATHS.CUSTOM_PAGE
    },
    {
      name: '利用規約',
      path: PATHS.CUSTOM_PAGE
    },
    {
      name: '個人情報の取扱について',
      path: PATHS.CUSTOM_PAGE
    },
    {
      name: '特定商取引法に基づく表記',
      path: PATHS.CUSTOM_PAGE
    },
    {
      name: 'お問い合わせ',
      path: PATHS.CUSTOM_PAGE
    },
  ]

  return (
    <footer id="footer">
      <Container>
        <Nav as="ul">
          {
            navList.map((item, index) =>
              <Nav.Item as="li" key={index}>
                <Nav.Link href={item.path}>
                  {item.name}
                </Nav.Link>
              </Nav.Item>
            )
          }
        </Nav>
      </Container>
    </footer>
  )
}

export default Footer;
