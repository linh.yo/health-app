import React from 'react'
import Header from 'layouts/Header'
import Footer from 'layouts/Footer'
import ScrollToTop from 'layouts/ScrollToTop'

type Props = {
  children: JSX.Element | JSX.Element[]
};

const MainLayout = ({ children }: Props) => {

  return (
    <React.Fragment>
      <Header />
      <main>
        {children}
      </main>
      <ScrollToTop />
      <Footer />
    </React.Fragment>
  );
};

export default MainLayout;
