import React, { useState } from 'react'
import { Nav, Navbar, Collapse, NavbarToggler, NavbarBrand, NavItem } from 'reactstrap'
import Logo from 'assets/images/logo.svg'
import { PATHS } from 'constants/paths'
import { NavLink } from 'react-router-dom'
import { IconMemo, IconChallenge, IconInfo, IconClose, IconMenu } from 'components/Icon'
import { Badge, Dropdown } from 'react-bootstrap'
import { INav } from 'types/common'
import useWindowDimensions from 'utils/hooks/useWindowDimensions'

const Header = () => {

  const { width } = useWindowDimensions()
  const [collapsed, setCollapsed] = useState<boolean>(true)
  const [opened, setOpened] = useState<boolean>(false)
  let notiNumber = 1;

  const toggleNavbar = () => setCollapsed(!collapsed)

  const navList:INav[] = [
    {
      name: '自分の記録',
      icon: IconMemo,
      path: PATHS.RECORD_PAGE,
      visible: true
    },
    {
      name: 'チャレンジ',
      icon: IconChallenge,
      path: PATHS.CHALLENGE_PAGE,
      visible: true
    },
    {
      name: 'お知らせ',
      icon: IconInfo,
      path: PATHS.NOTICE_PAGE,
      visible: true
    },
    {
      name: '自分の記録',
      path: PATHS.CUSTOM_PAGE,
      visible: false
    },
    {
      name: '体重グラフ',
      path: PATHS.CUSTOM_PAGE,
      visible: false
    },
    {
      name: '目標',
      path: PATHS.CUSTOM_PAGE,
      visible: false
    },
    {
      name: '選択中のコース',
      path: PATHS.CUSTOM_PAGE,
      visible: false
    },
    {
      name: 'コラム一覧',
      path: PATHS.CUSTOM_PAGE,
      visible: false
    },
    {
      name: '設定',
      path: PATHS.CUSTOM_PAGE,
      visible: false
    }
  ]

  const onToggleDropdownMenu = (isOpen: boolean) => {
    setOpened(isOpen)
  }

  const toggleNavbarOnMobile = () => {
    if (width < 600) {
      toggleNavbar()
    }
  }

  return (
    <header id="header">
      <Navbar container
              expand="md"
              fixed="top"
              light>
        <NavbarBrand href={PATHS.HOME_PAGE}>
          <img src={Logo} alt="Health App" />
        </NavbarBrand>
        <NavbarToggler onClick={toggleNavbar} />
        <Collapse isOpen={!collapsed} navbar>
          <Nav navbar>
            {
              navList.map((item, index) =>
                <NavItem key={index} className={item.visible ? '' : 'd-block d-sm-none'}>
                  <NavLink to={item.path}
                           className={({ isActive }) =>
                             isActive ? 'nav-link active' : 'nav-link'
                           }
                           onClick={toggleNavbarOnMobile}
                  >
                    {
                      item?.icon && <i>
                        {item?.icon}
                        {
                          item.path === PATHS.NOTICE_PAGE &&
                          <Badge bg="primary">{notiNumber}</Badge>
                        }
                      </i>
                    }
                    {item.name}
                  </NavLink>
                </NavItem>
              )
            }
          </Nav>
        </Collapse>
        <Dropdown className="d-none d-sm-block"
                  onToggle={onToggleDropdownMenu}
                  drop="start">
          <Dropdown.Toggle>
            {opened ? IconClose : IconMenu}
          </Dropdown.Toggle>

          <Dropdown.Menu>
            {
              navList.slice(3, navList.length).map((item, index) =>
                <Dropdown.Item href={item.path} key={index}>
                  {item.name}
                </Dropdown.Item>
              )
            }
          </Dropdown.Menu>
        </Dropdown>
      </Navbar>
    </header>
  )
}

export default Header
