import React, { useEffect } from 'react'
import './App.css'
import 'assets/styles/main.scss'
import AppRoute from './routes'
import AOS from 'aos'
import 'aos/dist/aos.css'

function App() {
  useEffect(() => {
    AOS.init()
    AOS.refresh()
  }, [])

  return (
    <AppRoute />
  );
}

export default App;
