import React, { ReactElement } from 'react'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import { PATHS } from 'constants/paths'
import MainLayout from 'layouts/Main'
import HomePage from 'pages/HomePage'
import RecordPage from 'pages/RecordPage'
import ColumnPage from 'pages/ColumnPage'

const AppRoute: React.FC = (): ReactElement => {
  return (
    <Router>
      <MainLayout>
        <Routes>
          <Route path={PATHS.HOME_PAGE} element={<HomePage />} />
          <Route path={PATHS.RECORD_PAGE} element={<RecordPage />} />
          <Route path={PATHS.COLUMN_PAGE} element={<ColumnPage />} />
        </Routes>
      </MainLayout>
    </Router>
  )
};

export default AppRoute
