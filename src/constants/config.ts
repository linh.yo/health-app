export const chartOptions = {
  animate: true,
  axisLeft: null,
  enableGridY: false,
  pointLabelYOffset: 0,
  lineWidth: 3,
  pointSize: 10,
  useMesh: true,
  colors: ['#8FE9D0', '#FFCC21'],
  theme: {
    fontSize: 11,
    textColor: '#fff',
    grid: {
      line: {
        stroke: '#777',
        strokeWidth: 1
      }
    }
  }
}
