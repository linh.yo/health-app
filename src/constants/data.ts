import Column_Image_1 from 'assets/images/sample/column-1.jpg'
import Column_Image_2 from 'assets/images/sample/column-2.jpg'
import Column_Image_3 from 'assets/images/sample/column-3.jpg'
import Column_Image_4 from 'assets/images/sample/column-4.jpg'
import Column_Image_5 from 'assets/images/sample/column-5.jpg'
import Column_Image_6 from 'assets/images/sample/column-6.jpg'
import Column_Image_7 from 'assets/images/sample/column-7.jpg'
import Column_Image_8 from 'assets/images/sample/column-8.jpg'
import { INewsItem } from 'types/column'

export const newsItems:INewsItem[] = [
  {
    id: 1,
    image: Column_Image_1,
    date: '2021.05.17',
    time: '23:25',
    title: '魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…',
    tags: ['魚料理', '和食', 'DHA']
  },
  {
    id: 2,
    image: Column_Image_2,
    date: '2021.05.17',
    time: '23:25',
    title: '魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…',
    tags: ['魚料理', '和食', 'DHA']
  },
  {
    id: 3,
    image: Column_Image_3,
    date: '2021.05.17',
    time: '23:25',
    title: '魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…',
    tags: ['魚料理', '和食', 'DHA']
  },
  {
    id: 4,
    image: Column_Image_4,
    date: '2021.05.17',
    time: '23:25',
    title: '魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…',
    tags: ['魚料理', '和食', 'DHA']
  },
  {
    id: 5,
    image: Column_Image_5,
    date: '2021.05.17',
    time: '23:25',
    title: '魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…',
    tags: ['魚料理', '和食', 'DHA']
  },
  {
    id: 6,
    image: Column_Image_6,
    date: '2021.05.17',
    time: '23:25',
    title: '魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…',
    tags: ['魚料理', '和食', 'DHA']
  },
  {
    id: 7,
    image: Column_Image_7,
    date: '2021.05.17',
    time: '23:25',
    title: '魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…',
    tags: ['魚料理', '和食', 'DHA']
  },
  {
    id: 8,
    image: Column_Image_8,
    date: '2021.05.17',
    time: '23:25',
    title: '魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…',
    tags: ['魚料理', '和食', 'DHA']
  }
]

export const yearData = [
  {
    id: "year-1",
    data: [
      { x: '6月', y: 1700 },
      { x: '7月', y: 1600 },
      { x: '8月', y: 1250 },
      { x: '9月', y: 1200 },
      { x: '10月', y: 850 },
      { x: '11月', y: 800 },
      { x: '12月', y: 500 },
      { x: '1月', y: 450 },
      { x: '2月', y: 400 },
      { x: '3月', y: 200 },
      { x: '4月', y: 150 },
      { x: '5月', y: 100 }
    ]
  },
  {
    id: "year-2",
    data: [
      { x: '6月', y: 1700 },
      { x: '7月', y: 1650 },
      { x: '8月', y: 1150 },
      { x: '9月', y: 1300 },
      { x: '10月', y: 1250 },
      { x: '11月', y: 900 },
      { x: '12月', y: 1200 },
      { x: '1月', y: 850 },
      { x: '2月', y: 700 },
      { x: '3月', y: 600 },
      { x: '4月', y: 500 },
      { x: '5月', y: 300 }
    ]
  }
]

export const monthData = [
  {
    id: "month-1",
    data: [
      { x: '1日', y: 500 },
      { x: '2日', y: 700 },
      { x: '3日', y: 450 },
      { x: '4日', y: 1000 },
      { x: '5日', y: 800 },
      { x: '6日', y: 600 },
      { x: '7日', y: 650 },
      { x: '8日', y: 550 },
      { x: '9日', y: 400 },
      { x: '10日', y: 200 },
      { x: '11日', y: 300 },
      { x: '12日', y: 200 },
      { x: '13日', y: 500 },
      { x: '14日', y: 700 },
      { x: '15日', y: 450 },
      { x: '16日', y: 1000 },
      { x: '17日', y: 800 },
      { x: '18日', y: 600 },
      { x: '19日', y: 650 },
      { x: '20日', y: 550 },
      { x: '21日', y: 400 },
      { x: '22日', y: 200 },
      { x: '23日', y: 300 },
      { x: '24日', y: 200 }
    ]
  },
  {
    id: "month-2",
    data: [
      { x: '1日', y: 300 },
      { x: '2日', y: 200 },
      { x: '3日', y: 60 },
      { x: '4日', y: 500 },
      { x: '5日', y: 800 },
      { x: '6日', y: 900 },
      { x: '7日', y: 500 },
      { x: '8日', y: 750 },
      { x: '9日', y: 850 },
      { x: '10日', y: 550 },
      { x: '11日', y: 400 },
      { x: '12日', y: 800 },
      { x: '13日', y: 300 },
      { x: '14日', y: 200 },
      { x: '15日', y: 60 },
      { x: '16日', y: 500 },
      { x: '17日', y: 800 },
      { x: '18日', y: 900 },
      { x: '19日', y: 500 },
      { x: '20日', y: 750 },
      { x: '21日', y: 850 },
      { x: '22日', y: 550 },
      { x: '23日', y: 400 },
      { x: '24日', y: 800 }
    ]
  }
]

export const weekData = [
  {
    id: "week-1",
    data: [
      { x: '1日', y: 500 },
      { x: '2日', y: 700 },
      { x: '3日', y: 450 },
      { x: '4日', y: 1000 },
      { x: '5日', y: 800 },
      { x: '6日', y: 600 },
      { x: '7日', y: 650 }
    ]
  },
  {
    id: "week-2",
    data: [
      { x: '1日', y: 300 },
      { x: '2日', y: 200 },
      { x: '3日', y: 60 },
      { x: '4日', y: 500 },
      { x: '5日', y: 800 },
      { x: '6日', y: 900 },
      { x: '7日', y: 500 }
    ]
  }
]

export const dayData = [
  {
    id: "day-1",
    data: [
      { x: '1時', y: 50 },
      { x: '2時', y: 70 },
      { x: '3時', y: 45 },
      { x: '4時', y: 100 },
      { x: '5時', y: 80 },
      { x: '6時', y: 60 },
      { x: '7時', y: 65 },
      { x: '8時', y: 55 },
      { x: '9時', y: 40 },
      { x: '10時', y: 20 },
      { x: '11時', y: 30 },
      { x: '12時', y: 20 }
    ]
  },
  {
    id: "day-2",
    data: [
      { x: '1時', y: 30 },
      { x: '2時', y: 20 },
      { x: '3時', y: 60 },
      { x: '4時', y: 50 },
      { x: '5時', y: 80 },
      { x: '6時', y: 90 },
      { x: '7時', y: 50 },
      { x: '8時', y: 75 },
      { x: '9時', y: 85 },
      { x: '10時', y: 55 },
      { x: '11時', y: 40 },
      { x: '12時', y: 80 }
    ]
  }
]

export const exerciseData = [
  {
    id: 1,
    content: '家事全般（立位・軽い）',
    duration: 10,
    kilocalories: 26
  },
  {
    id: 2,
    content: '家事全般（立位・軽い）',
    duration: 10,
    kilocalories: 26
  },
  {
    id: 3,
    content: '家事全般（立位・軽い）',
    duration: 10,
    kilocalories: 26
  },
  {
    id: 4,
    content: '家事全般（立位・軽い）',
    duration: 10,
    kilocalories: 26
  },
  {
    id: 5,
    content: '家事全般（立位・軽い）',
    duration: 10,
    kilocalories: 26
  },
  {
    id: 6,
    content: '家事全般（立位・軽い）',
    duration: 10,
    kilocalories: 26
  },
  {
    id: 7,
    content: '家事全般（立位・軽い）',
    duration: 10,
    kilocalories: 26
  },
  {
    id: 8,
    content: '家事全般（立位・軽い）',
    duration: 10,
    kilocalories: 26
  },
  {
    id: 9,
    content: '家事全般（立位・軽い）',
    duration: 10,
    kilocalories: 26
  },
  {
    id: 10,
    content: '家事全般（立位・軽い）',
    duration: 10,
    kilocalories: 26
  },
  {
    id: 11,
    content: '家事全般（立位・軽い）',
    duration: 10,
    kilocalories: 26
  },
  {
    id: 12,
    content: '家事全般（立位・軽い）',
    duration: 10,
    kilocalories: 26
  },
  {
    id: 13,
    content: '家事全般（立位・軽い）',
    duration: 10,
    kilocalories: 26
  },
  {
    id: 14,
    content: '家事全般（立位・軽い）',
    duration: 10,
    kilocalories: 26
  },
  {
    id: 15,
    content: '家事全般（立位・軽い）',
    duration: 10,
    kilocalories: 26
  },
  {
    id: 16,
    content: '家事全般（立位・軽い）',
    duration: 10,
    kilocalories: 26
  },
  {
    id: 17,
    content: '家事全般（立位・軽い）',
    duration: 10,
    kilocalories: 26
  },
  {
    id: 18,
    content: '家事全般（立位・軽い）',
    duration: 10,
    kilocalories: 26
  },
  {
    id: 19,
    content: '家事全般（立位・軽い）',
    duration: 10,
    kilocalories: 26
  },
  {
    id: 20,
    content: '家事全般（立位・軽い）',
    duration: 10,
    kilocalories: 26
  }
]

export const diaryData = [
  {
    id: 1,
    date: '2021.05.21',
    time: '23:25',
    content: '<p>私の日記の記録が一部表示されます。</p>' +
      '<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…</p>'
  },
  {
    id: 2,
    date: '2021.05.21',
    time: '23:25',
    content: '<p>私の日記の記録が一部表示されます。</p>' +
      '<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…</p>'
  },
  {
    id: 3,
    date: '2021.05.21',
    time: '23:25',
    content: '<p>私の日記の記録が一部表示されます。</p>' +
      '<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…</p>'
  },
  {
    id: 4,
    date: '2021.05.21',
    time: '23:25',
    content: '<p>私の日記の記録が一部表示されます。</p>' +
      '<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…</p>'
  },
  {
    id: 5,
    date: '2021.05.21',
    time: '23:25',
    content: '<p>私の日記の記録が一部表示されます。</p>' +
      '<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…</p>'
  },
  {
    id: 6,
    date: '2021.05.21',
    time: '23:25',
    content: '<p>私の日記の記録が一部表示されます。</p>' +
      '<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…</p>'
  },
  {
    id: 7,
    date: '2021.05.21',
    time: '23:25',
    content: '<p>私の日記の記録が一部表示されます。</p>' +
      '<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…</p>'
  },
  {
    id: 8,
    date: '2021.05.21',
    time: '23:25',
    content: '<p>私の日記の記録が一部表示されます。</p>' +
      '<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…</p>'
  }
]
