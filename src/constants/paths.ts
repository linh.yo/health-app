export const BASE_PATH = "/";

export const PATHS = {
  HOME_PAGE: BASE_PATH,
  RECORD_PAGE: BASE_PATH + "record",
  COLUMN_PAGE: BASE_PATH + "column",
  CHALLENGE_PAGE: BASE_PATH + "challenge",
  NOTICE_PAGE: BASE_PATH + "notice",
  CUSTOM_PAGE: BASE_PATH + "custom"
};
