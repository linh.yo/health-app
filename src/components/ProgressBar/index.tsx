import React, { useState, useEffect } from 'react'
import { IProgressBar } from 'types/common'
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar'
import 'react-circular-progressbar/dist/styles.css'

const ProgressBar: React.FC<IProgressBar> = ({ interval }) => {
  const [currentIndex, setCurrentIndex] = useState<number>(0)
  let percentages = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
  let intervalItem = interval / percentages.length

  useEffect(() => {
    const intervalNum = setInterval(() => {
      setCurrentIndex((currentIndex + 1) % percentages.length)
    }, intervalItem)

    return () => clearInterval(intervalNum)
  }, [currentIndex])

  const getCurrentPercentage = () => {
    return percentages[currentIndex]
  }

  return (
    <div className="progress-bar">
      <CircularProgressbar value={getCurrentPercentage()}
                           text={`${getCurrentPercentage()}%`}
                           strokeWidth={3}
                           styles={buildStyles({
                             textSize: '22px',
                             pathColor: '#fff',
                             textColor: '#fff',
                             trailColor: 'transparent'
                           })} />
    </div>
  )
}

export default ProgressBar
